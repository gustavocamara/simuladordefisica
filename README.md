Trabalho 

PARTE 1

O objetivo deste projeto e criar um programa que ao ser acessado pelo usuario ele movimenta bolas na tela

PARTE 2 (Spring-Mass)

Esta parte do projeto trata-se de uma simulação dinâmica mais complexa; o Spring-Mass composto por 
duas massas e uma mola .

Spring::Spring(Mass * mass1, Mass * mass2, double length, double stiffness, double damping)
 mass1(mass1), mass2(mass2), naturalLength(length), stiffness(stiffness), damping(damping)
 { }
 
Inclua uma breve descrição do que cada arquivo contém ou faz.
Inclua o diagrama das classes. Ou melhor, substitua o diagrama anterior por um novo diagrama em que são adiciondas as novas classes, 
mostrando as classes de Bouncing ball no mesmo diagrama que as classes do Spring-Mass.
Inclua também um pequeno exemplo mostrando as saídas geradas na Task 15 (coordenadas das massas).
Inclua uma figura que permite a visualização da trajetória das massas. Essa figura pode ser gerada, por exemplo, em Octave ou Python.





PARTE 3
Introduza o trabalho, falando sobre as novas funcionanalidades.
Indique quais são as dependências do seu programa, i.e., mencione OpenGL e Glut, incluindo ponteiros (links) para auxiliar os usuários a instalar essas bibliotecas.
Inclua a linha de compilação do programa test-springmass-graphics.cpp (command line). O arquivo Makefile te ajudará, gerando essa linha automaticamente.
Inclua uma breve descrição do que cada arquivo relacionado a esta parte do trabalho contém.
Inclua o diagrama das classes. Ou melhor, substitua o diagrama anterior, adicionando as novas classes, ou seja, mostre as classes de Bouncing ball e Spring-Mass no mesmo diagrama que as classes de Graphics.
Inclua um diagrama de sequencia mostrando como funciona a interação entre a interface gráfica e as outras classes a cada atualização da simulação e de sua visualização. 
Inclua também uma captura de tela (screen shot), mostrando seu programa em funcionamento.
